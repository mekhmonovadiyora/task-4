#  Web-app for user management

- task 4

## Features

- Docker for infrastructure
- Google Cloud Platform (Virtual machine) for server
- Node version 19.7.0
- React for frontend
- Express js for backend
- postgresql for database
- TypeORM for orm
- Dadabase diagramm https://dbdiagram.io/d/640947ff296d97641d868893
- ChakraUI for ui library
- TailwindCSS for css framework
