import { Request, Response } from "express";
import { verify } from "jsonwebtoken";
import { AppDataSource } from "../data-source";
import { User } from "../entity/User";

export default function (req: Request, res: Response, next: Function) {
	req.headers.authorization = req.headers.authorization?.trim();

	if (!req.headers.authorization) {
		console.log("no auth");
		return res.status(403).json({ message: "auth required" });
	}

	let tokenArr = req.headers.authorization.split(" ");

	if (tokenArr.length < 2 || tokenArr[0] != "Bearer") {
		return res.status(403).json({ message: "invalid token" });
	}

	let jwt = verify(tokenArr[1], "secret");

	if (!jwt) {
		return res.status(403).json({ message: "invalid token" });
	}

	const userRepo = AppDataSource.getRepository(User);

	req.user.id = JSON.parse(JSON.stringify(jwt)).id;

	const user = userRepo.findOne({
		where: {
			id: req.user.id,
			status: "active",
		},
	});

	if (!user) {
		return res.status(403).json({ message: "invalid token" });
	}

	return next();
}
