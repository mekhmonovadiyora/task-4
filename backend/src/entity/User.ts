import {
	Entity,
	PrimaryGeneratedColumn,
	Column,
	CreateDateColumn,
	DeleteDateColumn,
	ManyToMany,
	OneToOne,
	JoinColumn,
} from "typeorm";

@Entity()
export class User {
	@PrimaryGeneratedColumn()
	id: number;

	@Column()
	name: string;

	@Column()
	email: string;

	@Column()
	password: string;

	@Column({
		type: "enum",
		enum: ["active", "blocked"],
		default: "active",
	})
	status: "active" | "blocked";

	@Column({
		name: "last_login",
	})
	lastLogin: Date;

	@CreateDateColumn({
		name: "created_at",
	})
	createdAt: Date;

	@DeleteDateColumn({
		name: "deleted_at",
	})
	deletedAt: Date;

	@OneToOne(
		() => {
			return User;
		},
		{
			nullable: true,
		}
	)
	@JoinColumn({ name: "blocked_by" })
	blockedBy: User;

	@OneToOne(
		() => {
			return User;
		},
		{
			nullable: true,
		}
	)
	@JoinColumn({ name: "deleted_by" })
	deletedBy: User;
}
