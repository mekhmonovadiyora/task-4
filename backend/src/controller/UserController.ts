import { AppDataSource } from "../data-source";
import { NextFunction, Request, Response } from "express";
import { User } from "../entity/User";
import { compare, hash } from "bcrypt";
import { sign } from "jsonwebtoken";
import { In } from "typeorm";

export class UserController {
	private userRepository = AppDataSource.getRepository(User);

	async all(request: Request, response: Response, next: NextFunction) {
		return this.userRepository.find();
	}

	async login(request: Request, response: Response, next: NextFunction) {
		const { email, password } = request.body;

		const user = await this.userRepository.findOne({
			where: { email, status: "active" },
		});

		if (!user) {
			return response
				.status(401)
				.json({ message: "invalid email or password" });
		}

		const isPasswordValid = await compare(password, user.password);

		if (!isPasswordValid) {
			return response
				.status(401)
				.json({ message: "invalid email or password" });
		}

		const jwtToken = sign(
			{
				id: user.id,
			},
			"secret",
			{
				expiresIn: "72h",
			}
		);

		return response.json({
			token: jwtToken,
		});
	}

	async register(request: Request, response: Response, next: NextFunction) {
		const { name, email, password, confirmPassword } = request.body;

		if (password !== confirmPassword) {
			return response.status(400).json({ message: "passwords do not match" });
		}

		const user = await this.userRepository.findOne({
			where: { email },
		});

		if (user) {
			return response.status(400).json({ message: "email already exists" });
		}

		const hashPassword = await hash(password, 10);

		const newUser = Object.assign(new User(), {
			name,
			email: email,
			password: hashPassword,
		});

		let res = await this.userRepository.save(newUser);

		const jwtToken = sign(
			{
				id: newUser.id,
			},
			"secret",
			{
				expiresIn: "24h",
			}
		);

		return response.json({
			token: jwtToken,
		});
	}

	//block multiple users
	async block(request: Request, response: Response, next: NextFunction) {
		const { ids } = request.body;
		const blockedBy = request.user.id;

		const users = await this.userRepository.find({
			where: {
				id: In(ids),
				status: "active",
			},
		});

		if (!users) {
			return response.status(400).json({ message: "users not found" });
		}

		users.forEach((user) => {
			user.status = "blocked";
			user.blockedBy = blockedBy;
		});

		await this.userRepository.save(users);

		return response.json({ message: "users blocked" });
	}

	// delete multiple users

	async delete(request: Request, response: Response, next: NextFunction) {
		const { ids } = request.body;
		const deletedBy = request.user.id;

		const users = await this.userRepository.find({
			where: {
				id: In(ids),
			},
		});

		if (!users) {
			return response.status(400).json({ message: "users not found" });
		}

		users.forEach((user) => {
			user.deletedBy = deletedBy;
		});

		await this.userRepository.softRemove(users);

		return response.json({ message: "users deleted" });
	}

	// unblock multiple users
	async unblock(request: Request, response: Response, next: NextFunction) {
		const { ids } = request.body;

		const users = await this.userRepository.find({
			where: {
				id: In(ids),
				status: "blocked",
			},
		});

		if (!users) {
			return response.status(400).json({ message: "users not found" });
		}

		users.forEach((user) => {
			user.status = "active";
		});

		await this.userRepository.save(users);

		return response.json({ message: "users unblocked" });
	}
}
