import { UserController } from "./controller/UserController";
import AuthMiddleware from "./middleware/AuthMiddleware";

export const Routes = [
	{
		method: "get",
		route: "/users",
		controller: UserController,
		action: "all",
		middlewares: [AuthMiddleware],
	},
	{
		method: "post",
		route: "/users/:id",
		controller: UserController,
		action: "one",
		middlewares: [],
	},
	{
		method: "post",
		route: "/users",
		controller: UserController,
		action: "save",
		middlewares: [],
	},
	{
		method: "delete",
		route: "/users/:id",
		controller: UserController,
		action: "remove",
		middlewares: [],
	},
];
